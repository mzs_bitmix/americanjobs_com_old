<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/', 'PageController@home');
Route::get('/my.job/contactUs', ['as' => 'contact', 'uses' =>  'PageController@contact']);
Route::get('/my.job/aboutUs', ['as' => 'about', 'uses' =>  'PageController@about']);
Route::get('/my.job/privacyPolicy', ['as' => 'privacy', 'uses' =>  'PageController@privacy']);
Route::get('/my.job/termsOfUse', ['as' => 'terms', 'uses' =>  'PageController@terms']);
Route::get('/my.job/sitemap', ['as' => 'terms', 'uses' =>  'PageController@sitemap']);
Route::get('/my.job/browseJobs', ['as' => 'browsejobs', 'uses' =>  'PageController@browsejobs']);
Route::get('/my.job/searchJobsByCategory', ['as' => 'searchByCategory', 'uses' =>  'PageController@searchbycategory']);
Route::get('/my.job/searchJobsByState', ['as' => 'searchByState', 'uses' =>  'PageController@searchbystate']);
Route::get('/my.job/searchJobsInTopCities', ['as' => 'searchByCity', 'uses' =>  'PageController@searchbytopcity']);
Route::get('/my.job/searchJobsPopular', ['as' => 'searchByTitle', 'uses' =>  'PageController@searchbytoptitle']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout'); //Just added to fix issue
Route::get('/my.job/searchJobs', 'SearchController@index');
Route::post('/my.job/searchJobs', 'SearchController@index');
Route::get('/my.job/searchJobsControl', 'SearchController@index');
Route::post('/my.job/searchJobsControl', 'SearchController@index');
Route::get('/my.job/register', 'Auth\RegisterController@showRegistrationForm');
Route::post('/my.job/register', 'Auth\RegisterController@register');


//Route::get('/login', function () {return view('auth.login',['viewName'=>'login']);});
//Route::post('/my.job/login', 'Auth\LoginController@authentication')->name('login');
