@extends('layouts.master')

@section('content')

<div class="slimReg" id="supportLead">
    <div id="supportText">
    <h3>Privacy Policy</h3>
    <h4>AmericanJobs.com's Privacy Information - Overview</h4>
    <p>AmericanJobs.com is committed to protecting the privacy of our users. We want to provide a safe, secure user experience. We will use our best efforts to ensure that the information you submit to us remains private, and is used only for the purposes as set forth herein. The following reflects our commitment to you.</p>
    <h4>Information About All AmericanJobs.com Visitors</h4>
    <p>We gather information about all of our users collectively, such as what areas users visit most frequently and what services users access the most. We use such information in the aggregate and not on an individual basis. This information helps us determine what is most beneficial for our users, and how we can develop a better overall website for our users. This information may be shared with our partners, but only in the aggregate, so that they too may develop a better overall career website for you, as well.</p>
    <h4>Information About You Specifically</h4>
    <p>In some instances, such as when you sign up to use a service, accept a special offer, request more information from an advertiser, enter into a contest, or purchase a product, we may need more specific personal information about you, such as name, address, e-mail address, telephone number, credit card number, etc. We may use that information to make you aware of additional products and services which may be of interest to you, or to contact you regarding site changes. We may also ask you for other information, such as feedback regarding the site, the types of jobs you are interested in, etc. Again, in an effort to develop and deliver to you the best possible career website.</p>
    <h4>General Information Disclosure</h4>
    <p>We do not disclose information about your individual visits to AmericanJobs.com, or personal information that you provide, such as your name, address, e-mail address, telephone number, credit card number, etc., to any outside parties, except when we believe the law requires it, if it is needed to fulfill a product purchase, special offer request, contest entry etc., or as otherwise described in this Privacy Policy. But, we may record and share aggregated information with our partners. In addition, if we sell all or part of our business or make a sale or transfer of all or a material part of our assets or are otherwise involved in a merger or transfer of all or a material part of our business, we may transfer your personal information to the party or parties involved as part of that transaction.</p>
    <h4>Privacy Commitment Changes</h4>
    <p>If we decide to change our privacy commitment for AmericanJobs.com, we will post those changes here so that you will always know what information we gather, how we might use that information, and whether we will disclose it to anyone. If, at any time, you have questions or concerns about AmericanJobs.com's privacy commitment, please feel free to email us at privacypolicy@AmericanJobs.com. Thank you for using AmericanJobs.com. We'll continue working to make this your favorite website to manage your career.</p>
    </div>
</div>

@endsection
