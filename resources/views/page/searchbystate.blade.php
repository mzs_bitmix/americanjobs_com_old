@extends('layouts.master')

@section('title', 'Jobs Listed by State')
@section('meta-description', 'Find jobs located in the United States.  Find the best local jobs by state at American Jobs.')
@section('meta-keywords', $metaKeywords)


@section('content')

<?php $half = ceil(count($stateList)/2);?>
<div class="slimReg" id="supportLead">
    <h3>Search American Jobs By State</h3>
    <p>
        <div style="float: left; width: 330px;">
            <ul class="referList">
        <?php
            $row = 0;
            $logged = Auth::check();
            foreach ($stateList as $id=>$state) {
                if ($row == $half) {
                    echo '</ul></div><div style="float:left"><ul class="referList">';
                }
                if (!$logged) {
                    echo '<li><a href="/my.job/searchJobsControl/?geoField='.$state.'" title="American Jobs In '.$state.'">Jobs In '.$state.'</a></li>';
                } else {
                    echo '<li><a href="/my.job/searchJobs/?geoField='.$state.'" title="American Jobs In '.$state.'">'.$state.'</a></li>';
                }
            $row++;
        }?>
            </ul>
        </div>
    </p>
</div>

@endsection

<cfloop query = "cachedUSstates">
    <cfset metaKeywords = listAppend (metaKeywords, "#cachedUSStates.stateName# jobs, jobs in #cachedUSStates.stateName#") />
</cfloop>
