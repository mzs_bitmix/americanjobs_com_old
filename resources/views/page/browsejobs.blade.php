@extends('layouts.master')

@section('content')

<div class="slimReg" id="supportLead">
    <h3>Top Job Titles By Category</h3>
    <p>
        <div style="width:90%;margin:auto;text-align:center;">
            <?php $row =0;
            foreach ($categoryList as $id=>$category) {
                echo '<a href="#'.preg_replace('/[^[:alpha:]]/', '', $category).'" title="'.$category.' Job Titles"  rel="nofollow">'.$category.'</a>';
                if ($row>0 && $row%4 == 0) {
                    echo '<br />';
                } else {
                    echo '&nbsp;|&nbsp;';
                }
                $row++;
            }
            ?>
        </div>
    </p>
    <p>
        <ul class="referList">
            <?php
            foreach ($categoryList as $id=>$category) {
                echo '<li><strong><a name="'.preg_replace('/[^[:alpha:]]/', '', $category).'">'.$category.'</a></strong></li>';
                echo '<ul>';
                $jobtitles = DB::table('americanjobs_jobtitles')->where('jobIndustryID', '=', $id)->orderByRaw('jobTitleCount desc, jobtitle asc')->limit(30)->get();
                foreach ($jobtitles as $jobtitle) {
                    echo '<li><a href="/my.job/searchJobsControl/keywordField='.$jobtitle->jobTitle.'" title="'.$jobtitle->jobTitle.' Jobs">'.$jobtitle->jobTitle.'</a></li>';
                }
                echo '</ul>';
            }
            ?>
        </ul>
    </p>
</div>

@endsection
