@extends('layouts.master')

@section('title', 'Jobs Listed by Category')
@section('meta-description', 'Search American Jobs by category.  Specific Industries are listed to help you find the best jobs in the United States by category.')


@section('content')
<?php $half = ceil(count($categoryList)/2);?>
    <div class="slimReg" id="supportLead">
        <h3>Search American Jobs By Category</h3>
        <p>
            <div style="float:left">
            <ul class="referList">
        <?php
            $row = 0;
            $logged = Auth::check();
            foreach ($categoryList as $id=>$category) {
                if ($row == $half) {
                    echo '</ul></div><div style="float:right"><ul class="referList">';
                }
                if (!$logged) {
                    echo '<li><a href="/my.job/searchJobsControl/?keywordField='.$category.'" title="Search '.$category.' Jobs in the United States">'.$category.' Jobs</a></li>';
                } else {
                    echo '<li><a href="/my.job/searchJobs/?keywordField='.$category.'" title="Search '.$category.' Jobs in the United States">'.$category.' Jobs</a></li>';
                }
            $row++;
        }?>
                </ul>
            </div>
        </p>
    </div>

@endsection
