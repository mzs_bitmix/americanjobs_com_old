@extends('layouts.master')

@section('title', 'Jobs Listed by State')
@section('meta-description', 'Find jobs located in the United States.  Find the best local jobs by state at American Jobs.')
@section('meta-keywords', '')


@section('content')

<?php $half = ceil(count($stateList)/2);?>
    <div class="slimReg" id="supportLead">
        <h3>Search American Jobs By City</h3>
        <p>
            <div style="float:left">
                <ul class="referList">
                <?php
                    $row = 0;
                    $logged = Auth::check();
                    foreach ($stateList as $id=>$state) {
                        if ($row == $half) {
                            echo '</ul></div><div style="float:left"><ul class="referList">';
                        }
                        if (!$logged) {
                            echo '<li><a href="/my.job/searchJobsControl/?geoField='.$state.'" title="American Jobs In '.$state.'">Jobs In '.$state.'</a></li>';
                        } else {
                            echo '<li><a href="/my.job/searchJobs/?geoField='.$state.'" title="American Jobs In '.$state.'">'.$state.'</a></li>';
                        }
                    $row++;
                }?>

                </ul>
            </div>
        </p>
    </div>


<div class="slimReg" id="supportLead">
    <h3>Search American Jobs By State</h3>
    <p>
        <div style="float: left; width: 330px;">
            <ul class="referList">
                <cfoutput query="getTopCitiesInUSA" maxrows="#getTopCitiesInUSA.recordCount/2#">
                    <li>
                    <cfset city = convertCase(getTopCitiesInUSA.city) />
                    <cfset state = convertCase (getTopCitiesInUSA.state) />
                        <cfif not loggedIn() AND NOT request.app.unuMode>
                            <a href="/my.job/searchJobsControl/keywordField=/geoField=#replace(variables.city,'/','~','ALL')#, #replace(variables.state,'/','~','ALL')#" title = "#variables.city#-#variables.state#-American Job Listings">
                                Jobs In #variables.city#, #variables.state#
                            </a>
                        <cfelse>
                            <a href="/my.job/searchJobs/keywordField=/geoField=#replace(variables.city,'/','~','ALL')#, #replace(variables.state,'/','~','ALL')#" title = "#variables.city#-#variables.state#-American Job Listings">
                                Jobs In #variables.city#, #variables.state#
                            </a>
                        </cfif>
                    </li>
                </cfoutput>
                </ul>
            </div>
            <div style="float:right">
                <ul class="referList">
                <cfoutput query="getTopCitiesInUSA" maxrows="#getTopCitiesInUSA.recordCount/2#" startrow="#getTopCitiesInUSA.recordCount/2+1#">
                    <li>
                    <cfset city = convertCase(getTopCitiesInUSA.city) />
                    <cfset state = convertCase (getTopCitiesInUSA.state) />
                        <cfif not loggedIn() AND NOT request.app.unuMode>
                            <a href="/my.job/searchJobsControl/keywordField=/geoField=#replace(variables.city,'/','~','ALL')#, #replace(variables.state,'/','~','ALL')#" title = "#variables.city#-#variables.state#-American Job Listings">
                                Jobs In #variables.city#, #variables.state#
                            </a>
                        <cfelse>
                            <a href="/my.job/searchJobs/keywordField=/geoField=#replace(variables.city,'/','~')#, #replace(variables.state,'/','~')#" title = "#variables.city#-#variables.state#-American Job Listings">
                                Jobs In #variables.city#, #variables.state#
                            </a>
                        </cfif>
                    </li>
                </cfoutput>
            </ul>
        </div>
    </p>
</div>

@endsection

<cfloop query = "cachedUSstates">
    <cfset metaKeywords = listAppend (metaKeywords, "#cachedUSStates.stateName# jobs, jobs in #cachedUSStates.stateName#") />
</cfloop>
<cfquery name="getTopCitiesInUSA" datasource="#request.app.validationDB#" cachedwithin="#CreateTimeSpan(0,6,0,0)#">
    select top 100 city, state, stateCode, count(zipCode)
    from valGeoData
    where state <> '' and stateCode <> 'PR'
    group by city, state, stateCode, population
    order by count(zipCode) desc
</cfquery>

<cfset htmlTitle = "American Jobs Listed by City" />
<cfset metaKeywords = "American Jobs By City" />
<cfloop query = "getTopCitiesInUSA">
    <cfset city = convertCase(getTopCitiesInUSA.city) />
    <cfset metaKeywords = metaKeywords & ", #variables.city# #getTopCitiesInUSA.state# jobs" />
</cfloop>
<cfset metaDescription = "Search and apply to top jobs in the United States by city at American Jobs (AmericanJobs.com)" />
