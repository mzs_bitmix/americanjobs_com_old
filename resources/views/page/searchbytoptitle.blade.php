@extends('layouts.master')

@section('title', 'Popular American Job Listings')
@section('meta-description', 'Find the jobs everybody is looking for at American Jobs (AmericanJobs.com).  Start your search with the same searches other people find the best jobs with.')

@section('content')

    <div class="slimReg" id="supportLead">
        <h3>Find American Jobs That Others Are Looking For</h3>
        <p>
            <h4>Top Searchs By Keyword And Location</h4>
            <table width="100%">
                <tr>
            <cfoutput query="getTopSearchesByKeywordAndGeo">
                <cfset searchPhrase = "" />
                <cfif trim(spKeyword) is not "">
                    <cfset searchPhrase = reReplaceNoCase(spKeyword,'field:','','ALL') & " Jobs  " />
                </cfif>
                <cfif trim(spGeo) is not "">
                    <cfif trim(searchPhrase) is not "">
                        <cfset searchPhrase = trim(searchPhrase) & " in " & spGeo />
                    <cfelse>
                        <cfset searchPhrase = spGeo & " Jobs " />
                    </cfif>
                </cfif>

                <cfif not loggedIn() AND NOT request.app.unuMode>
                    <td width="50%" style="font-size:12px;"><a href="/my.job/searchJobsControl/keywordField=#replace(spKeyword,'/','~','ALL')#/geoField=#replace(spGeo,'/','~','ALL')#"  alt="#searchPhrase#"  title="View #searchPhrase#">#searchPhrase#</a></td>
                <cfelse>
                    <td width="50%" style="font-size:12px;"><a href="/my.job/searchJobs/keywordField=#replace(spKeyword,'/','~','ALL')#/geoField=#replace(spGeo,'/','~','ALL')#" alt="#searchPhrase#" title="View #searchPhrase#">#searchPhrase#</a></td>
                </cfif>

                <cfif currentRow mod 2 is 0>
                    </tr>
                </cfif>
            </cfoutput>
            </table>
        </p>


        <p>
            <h4>Top Searchs By Keyword</h4>
            <table width="100%">
                <tr>
            <cfoutput query="getTopSearchesByKeyword">
                <cfset searchPhrase = "" />
                <cfif trim(spKeyword) is not "">
                    <cfset searchPhrase = "#reReplaceNoCase(spKeyword,'field:','','ALL')# Jobs  " />
                </cfif>
                <cfif trim(spGeo) is not "">
                    <cfif trim(searchPhrase) is not "">
                        <cfset searchPhrase = "#trim(searchPhrase)# in #spGeo#" />
                    <cfelse>
                        <cfset searchPhrase = "#spGeo# Jobs" />
                    </cfif>
                </cfif>

                <cfif not loggedIn() AND NOT request.app.unuMode>
                    <td width="50%" style="font-size:12px;"><a href="/my.job/searchJobsControl/keywordField=#replace(spKeyword,'/','~','ALL')#/geoField=#replace(spGeo,'/','~','ALL')#"  alt="#searchPhrase#"  title="View #searchPhrase#">#searchPhrase#</a></td>
                <cfelse>
                    <td width="50%" style="font-size:12px;"><a href="/my.job/searchJobs/keywordField=#replace(spKeyword,'/','~','ALL')#/geoField=#replace(spGeo,'/','~','ALL')#" alt="#searchPhrase#" title="View #searchPhrase#">#searchPhrase#</a></td>
                </cfif>

                <cfif currentRow mod 2 is 0>
                    </tr>
                </cfif>
            </cfoutput>
            </table>
        </p>

        <p>
            <h4>Top Searchs By Location</h4>
            <table width="100%">
                <tr>
            <cfoutput query="getTopSearchesByGeo">
                <cfset searchPhrase = "" />
                <cfif trim(spKeyword) is not "">
                    <cfset searchPhrase = "#reReplaceNoCase(spKeyword,'field:','','ALL')# Jobs  " />
                </cfif>
                <cfif trim(spGeo) is not "">
                    <cfif trim(searchPhrase) is not "">
                        <cfset searchPhrase = "#trim(searchPhrase)# in #spGeo#" />
                    <cfelse>
                        <cfset searchPhrase = "#spGeo# Jobs" />
                    </cfif>
                </cfif>

                <cfif not loggedIn() AND NOT request.app.unuMode>
                    <td width="50%" style="font-size:12px;"><a href="/my.job/searchJobsControl/keywordField=#replace(spKeyword,'/','~','ALL')#/geoField=#replace(spGeo,'/','~','ALL')#"  alt="#searchPhrase#"  title="View #searchPhrase#">#searchPhrase#</a></td>
                <cfelse>
                    <td width="50%" style="font-size:12px;"><a href="/my.job/searchJobs/keywordField=#replace(spKeyword,'/','~','ALL')#/geoField=#replace(spGeo,'/','~','ALL')#" alt="#searchPhrase#" title="View #searchPhrase#">#searchPhrase#</a></td>
                </cfif>

                <cfif currentRow mod 2 is 0>
                    </tr>
                </cfif>
            </cfoutput>
            </table>
        </p>
    </div>

@endsection



<cfquery name="getTopSearchesByKeywordAndGeo" datasource="#request.app.dsMain#" cachedwithin="#CreateTimeSpan(0,3,0,0)#">
    select top 50 spKeyword, spGeo, count (spid)
    from tblSearchPerformed
    where spResultCount > 0 and (spKeyword <> '' and  spGeo <> '')
    group by spKeyword, spGeo
    order by count (spid) desc
</cfquery>

<cfquery name="getTopSearchesByGeo" datasource="#request.app.dsMain#" cachedwithin="#CreateTimeSpan(0,6,0,0)#">
    select top 50 spKeyword, spGeo, count (spid)
    from tblSearchPerformed
    where spResultCount > 0 and (spKeyword = '' and  spGeo <> '')
    group by spKeyword, spGeo
    order by count (spid) desc
</cfquery>

<cfquery name="getTopSearchesByKeyword" datasource="#request.app.dsMain#" cachedwithin="#CreateTimeSpan(0,2,0,0)#">
    select top 50 spKeyword, spGeo, count (spid)
    from tblSearchPerformed
    where spResultCount > 0 and (spKeyword <> '' and  spGeo = '')
    group by spKeyword, spGeo
    order by count (spid) desc
</cfquery>

<cfset metaKeywords = "" />
<cfloop query="getTopSearchesByKeywordAndGeo">
    <cfset searchTerms = spKeyword & spGeo />
    <cfset metaKeywords = listAppend (metaKeywords, searchTerms) />
</cfloop>
