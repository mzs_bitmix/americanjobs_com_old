@extends('layouts.master')

@section('content')

<div class="slimReg" id="supportLead">
        <div id="supportText">
            <h3>About AmericanJobs.com</h3>
            <p>
                North America's fastest growing career portal, <a href="http://www.job.com" title="Jobs at Job.com">Job.com</a> is pleased to introduce you to a new approach to job searching.
                AmericanJobs.com brings value and functionality to the world online job boards.
            </p>
            <p>
                The team behind AmericanJobs.com is dedicated to connecting people with great jobs along with the most time and cost-effective, career-advancing resources.
                It is our mission to develop and maintain a website where people can go to work!
            </p>
        </div>
</div>


@endsection
