@extends('layouts.master')

@section('content')

    <div class="slimReg" id="supportLead">
        <h3>AmericanJobs.com Sitemap</h3>
        <div class="siteMap">
            <ul>
                <li><a href="/my.job/register">Signup And Find Jobs In The United States!</a></li>
            </ul>
            <ul>
                <li><a href="/my.job/searchJobsByState">Find Jobs By State</a></li>
                <li><a href="/my.job/searchJobsByCategory">Find Jobs By Category</a></li>
                <li><a href="/my.job/searchJobsInTopCities">Find Jobs In The Top Cities In The United States</a></li>
                <li><a href="/my.job/searchJobsPopular">Find Jobs Using The Most Popular Recent Searches</a></li>
            </ul>

<?php
$baseUrl = env('APP_URL', 'https://www.americanjobs.com');
$link = $baseUrl.\App\Utility\Url::jobSearchUrl('', '#state#');
?>
                    <ul>
<?php
    $states = $stateDomain->getList();
    foreach ($states as $item) {
        $state = str_replace('/', ' - ', $item['StateName']);
        echo '<li><a href="'.str_replace('#state#', $state, $link).'">Jobs In '.$state.'</a></li>';
    }
?>
                    </ul>
<?php ?>
                    <ul>
<?php
$link = $baseUrl.\App\Utility\Url::jobSearchUrl('#keyword#', '');
    $categories = $categoryDomain->getList();
    foreach ($categories as $category) {
        echo '<li><a href="'.str_replace('#keyword#', str_replace('/', ' - ', $category), $link).'">'.$category.' Jobs</a></li>';
    }
?>
                    </ul>
<?php ?>
            <?php /*?>


                <!--- Generate sitemap section for category pages --->
                <cfquery name="getCategoriesForSearch" datasource="#request.app.dsMain#">
                    select distinct csDesc
                    from valCategorySearch
                    order by csDesc
                </cfquery>

                <cfsavecontent variable="categorySection">
                    <ul>
                    <cfloop query="getCategoriesForSearch">
                        <cfset locURL = "#request.app.websiteHome#/my.job/searchJobsControl/keywordField=field:#replace(getCategoriesForSearch.csDesc,'/','~','ALL')#" />
                        <li><a href="#locURL#">#getCategoriesForSearch.csDesc# Jobs</a></li>
                    </cfloop>
                    </ul>
                </cfsavecontent>


                <!--- Generate sitemap section for city pages --->
                <cfquery name="getTopCitiesInUSA" datasource="#request.app.validationDB#" cachedwithin="#CreateTimeSpan(0,6,0,0)#">
                    select top 100 city, state, stateCode, count(zipCode)
                    from valGeoData
                    where state <> '' and stateCode <> 'PR'
                    group by city, state, stateCode, population
                    order by count(zipCode) desc
                </cfquery>

                <cfsavecontent variable="citiesSection">
                    <ul>
                    <cfloop query="getTopCitiesInUSA">
                        <cfset locURL = "#request.app.websiteHome#/my.job/searchJobsControl/keywordField=/geoField=#replace(getTopCitiesInUSA.city,'/','~','ALL')#,#replace(getTopCitiesInUSA.state,'/','~','ALL')#" />
                        <!--- <cfset locURL = xmlFormat(locURL) /> --->
                        <li><a href="#locURL#">Jobs In #convertCase(getTopCitiesInUSA.city)#, #convertCase(getTopCitiesInUSA.state)#</a></li>
                    </cfloop>
                    </ul>
                </cfsavecontent>

                <!--- Generate GOOGLE sitemap section for popular searches pages --->
                <cfquery name="getTopSearches" datasource="#request.app.dsMain#">
                    select top 50 spKeyword, spGeo, count (spid)
                    from tblSearchPerformed
                    where spResultCount > 0 and (spKeyword <> '' or  spGeo <> '')
                    group by spKeyword, spGeo
                    order by count (spid) desc
                </cfquery>

                <cfsavecontent variable="popularSearchesSection">
                    <ul>
                    <cfloop query="getTopSearches">
                        <cfset locURL = "#request.app.websiteHome#/my.job/searchJobsControl/keywordField=#replace(getTopSearches.spKeyword,'/','~','ALL')#/geoField=#replace(getTopSearches.spGeo,'/','~','ALL')#" />
                        <cfset locURL = xmlFormat(locURL) />
                        <cfset geoDisp = getTopSearches.spGeo />
                        <cfif geoDisp is "">
                            <cfset geoDisp = "the United States" />
                        </cfif>
                        <li><a href="#locURL#">#reReplaceNoCase(getTopSearches.spKeyword, "^field:", "", "ALL")# Jobs in #geoDisp#</a></li>
                    </cfloop>
                    </ul>
                </cfsavecontent>


                #statesSection#
                #categorySection#
                #citiesSection#
                #popularSearchesSection#
            <?php */?>


            <ul>
                <li><a href="/my.job/aboutUs">About Us</a></li>
                <li><a href="/my.job/contactUs">Contact Us</a></li>
                <li><a href="/my.job/privacyPolicy">Privacy Policy</a></li>
                <li><a href="/my.job/termsOfUse">Terms of Use</a></li>
                <li><a href="/my.job/browseJobs">Browse Jobs</a></li>
                <li><a href="/my.job/sitemap">Site Map</a></li>
            </ul>
            <hr />
            <h4>AmericanJobs.com Network Sites</h4>
            <ul>
                <li class="listDesc">Here are some other sites that we partner with to help you find your dream job</li>
                <li><a href="http://www.job.com/?us=801" target="sitemapLinks">Job.com</a> - Looking for <a href="http://www.job.com/?us=801" target="sitemapLinks">jobs</a> in the United States?  Get found by employers hiring today!  Register and post your resume, search jobs, and find your next career here!</li>
                <li><a href="http://www.hotresumes.com/index.cfm?hot=60" target="sitemapLinks">HotResumes.com</a> is an economy level, Resume Database that Employers and Recruiters sign up to search over the Internet.</li>
                <li><a href="http://www.usJobBoard.com/" target="sitemapLinks">USJobBoard.com</a> is a general interest Job Board that connects US based Job Seekers with US based Companies and Employers offering <a href="http://www.usjobboard.com" target="sitemapLinks">local jobs</a>. The Job Board attracts a wide audience of users.</li>
                <li><a href="http://www.job.net/" target="sitemapLinks">Job.net</a> - What you need to know about the Job market.  <a href="http://www.job.net" target="sitemapLinks">Search Jobs</a> from all over the United States.</li>
                <li><a href="http://www.newsForRecruiters.com" target="sitemapLinks">NewsForRecruiters.com</a> is a bi-monthly eZine with relevant articles and information that is designed to help Recruiters stay on the forefront of Internet Based Recruiting.</li>
                <li><a href="http://www.jobSeekerNews.com/" target="sitemapLinks">JobSeekerNews.com</a> is the sister publication to News For Recruiters and is a bi-monthly eZine with articles and information for active Job Seekers.</li>
                <li><a href="http://www.resumezapper.com/index.cfm?rz=360" target="sitemapLinks">ResumeZapper.com</a> - Zap Your Resume to 1000's of Recruiters, Today!</li>
                <li><a href="http://www.resumereach.com/" target="sitemapLinks">ResumeReach</a> - Instantly post your resume to more than 90 career websites!</li>
                <li><a href="http://www.emailmyresume.com/index.cfm?hm=198" target="sitemapLinks">EmailMyResume</a> - Email your resume to 1000s of Top Companies & Recruiters!</li>
                <li><a href="http://www.careerDirectory.com/?cdn=#request.app.siteCommonName#" target="sitemapLinks">CareerDirectory.com</a> is a Search Engine and Directory Listing for Career Related Websites.</li>
                <li><a href="http://www.CanadianJobs.com" target="sitemapLinks">Canadian Jobs</a> - Find employment in Canada at CanadianJobs.com</li>
                <li><a href="http://www.HRJobNet.com" target="sitemapLinks">Human Resource Jobs</a></li>
                <li><a href="http://www.EmploymentCalifornia.com" target="sitemapLinks">Jobs In California</a> - Find employment in California at EmploymentCalifornia.com</li>
                <li><a href="http://www.MedicalCareerNet.com" target="sitemapLinks">Medical Careers</a></li>
                <li><a href="http://www.DCAreaJobs.com" target="sitemapLinks">DC Area Jobs</a> - Find employment in D.C. and surrounding areas at DCAreaJobs.com</li>

            </ul>
        </div>
    </div>


@endsection
