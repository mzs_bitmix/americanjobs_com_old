@extends('layouts.master')

@section('content')

    <div class="searchContainer">
        <div class="fieldContainer">


            <div class="searchFields">


            <form method="get" name="entry" action="<?php if (Auth::check()) { echo '/my.job/searchJobs';} else {echo '/my.job/register';}?>">
                <div class="keyword">
                    <label for="keywordField"><em>What</em> are you looking for?</label>
                    <input type="text" id="keywordField" name="keywordField" value="" tabindex="1" />
                    <h4 class="subNote">Keywords, companies, job titles, industries</h4>
                </div>
                <div class="geo">
                    <label for="geoField"><em>Where</em> do you want to look?</label>
                    <input type="text" id="geoField" name="geoField" value="" tabindex="2" />
                    <h4 class="subNote">City, State, Zip code</h4>
                </div>
                <div class="send">
                    <input type="image" class="getJobs" alt="Search Jobs" src="/images/btn_getJobs.gif" tabindex="3" />
                </div>
            </form>

            </div>
        </div>
    </div>
    <img src="images/spacer.gif" alt="" class="invisiShim" />

<div class="indexIntro">
    <h3>American Jobs is your source for top local jobs in the United States.</h3>
    <p> While offering some of the best employment opportunities in the United States, you are sure to find an American job and career you will love.</p>
    <p>Based on your U.S. job search criteria and your career profile, we'll match you with local jobs from some of the United States's leading employers.  You're only a few clicks away from accessing 1,000s of local job openings.</p>
    <p>Search and apply to great jobs and find employment in the United States now!</p>
</div>

@endsection
