@extends('layouts.master')

@section('content')

    <div class="initResults">
    <h3 class="searchHeader"><em><?php echo $searchHeaderText;?></em></h3>
        <div class="controlsTier">
            <div class="reSearch">
            <form method="get" name="entry" action="/my.job/searchJobs">
                <div class="keyword">
                    <label for="keywordField"><em>What</em> are you looking for?</label>
                    <input type="text" id="keywordField" name="keywordField" tabindex="1" value="<?php echo $keywords;?>" />
                    <h4 class="subNote">Keywords, Companies, Job Titles</h4>
                </div>
                <div class="geo">
                    <label for="keywordField"><em>Where</em> do you want to look?</label>
                    <input type="text" id="geoField" name="geoField" tabindex="2" value="<?php echo $location;?>" />
                    <h4 class="subNote">City, State, Zip</h4>
                </div>
                <div class="reSend">
                    <input type="submit" tabindex="3" value="Search" class="btn-search"/>
                </div>
            </form>
            </div>
        </div>
<?php if (!empty($jobs)) {?>

            <div class="resultsContainer">
                <div class="bookmarkBtn">
                <!--- Changes addThis for IE6 users--->
                    <div style="position:relative;top:0px;top:0px;right:0px;width:160px;*right:5px;margin-top: -5px;">
                        <script type="text/javascript">
                            addthis_url    = 'http://www.americanjobs.com/my.job/searchJobsControl/keywordField=<?php echo $keywords;?>&geoField=<?php echo $location;?>';
                            addthis_title  = '#htmlTitle#';
                            addthis_brand = "AmericanJobs.com ";
                            addthis_pub    = "vaweb2007";
                        </script>
                        <div class="addthis_toolbox addthis_default_style" style="float:right; margin-top:5px;">
                            <a class="addthis_button_facebook"></a>
                            <a class="addthis_button_delicious"></a>
                            <a class="addthis_button_email"></a>
                            <a class="addthis_button_favorites"></a>
                            <span class="addthis_separator">|</span>
                            <a href="http://addthis.com/bookmark.php?v=250&amp;pub=vaweb2007" class="addthis_button_expanded">More</a>
                        </div>
                        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js##pub=vaweb2007"></script>
                    </div>
                    <!--- AddThis Bookmark Button END --->
                </div>
                <table id="jobsTeaser">
                        <tr style="background: #0065A8; color: #fff;">
                            <td>
                                <dl>
                                    <dt>Displaying <strong><?php echo $dispRange;?></strong> of <?php echo $displayResultsStr;?> results.</dt>
                                    <dd class="pageControls">
                                    <?php if ($page!=0) {?>
                                        <a href="<?php echo $pageUrl.'&page=0';?>" class="pageBtn"><img src="/images/buttons/btn_first.gif" alt="First" border="0" /></a>
                                        <a href="<?php echo $pageUrl.'&page='.$prevPage;?>" class="pageBtn"><img src="/images/buttons/btn_prev.gif" alt="Prev" border="0" /></a>
                                    <?php } else {?>
                                        <a href="##" class="pageBtn"><img src="/images/buttons/btn_first_null.gif" alt="First" border="0" /></a>
                                        <a href="##" class="pageBtn"><img src="/images/buttons/btn_prev_null.gif" alt="First" border="0" /></a>
                                    <?php }?>
                                    <?php if ($page!=$lastPage) {?>
                                        <a href="<?php echo $pageUrl.'&page='.$nextPage;?>" class="pageBtn"><img src="/images/buttons/btn_next.gif" alt="Next" border="0" /></a>
                                        <a href="<?php echo $pageUrl.'&page='.$lastPage;?>" class="pageBtn"><img src="/images/buttons/btn_last.gif" alt="Last" border="0" /></a>
                                    <?php } else {?>
                                        <a href="##" class="pageBtn"><img src="/images/buttons/btn_next_null.gif" alt="Next" border="0" /></a>
                                        <a href="##" class="pageBtn"><img src="/images/buttons/btn_last_null.gif" alt="Last" border="0" /></a>
                                    <?php }?>
                                    </dd>
                                </dl>
                            </td>
                        </tr>
                        <?php foreach($jobs as $job) {?>
                        <tr>
                            <td>
                                <dl>
                                    <dt><a href="<?php echo $job->url;?>" target="_showJobWindow"><?php echo $job->title;?></a></dt>
                                    <dd><?php echo $job->city[0];?></dd>
                                </dl>
                            </td>
                        </tr>
                        <?php }?>
                        <tr style="background: #0065A8; color: #fff;">
                            <td>
                                <dl>
                                    <dt>Displaying <strong><?php echo $dispRange;?></strong> of <?php echo $displayResultsStr;?> results.</dt>
                                    <dd class="pageControls">
                                    <?php if ($page!=0) {?>
                                        <a href="<?php echo $pageUrl.'&page=0';?>" class="pageBtn"><img src="/images/buttons/btn_first.gif" alt="First" border="0" /></a>
                                        <a href="<?php echo $pageUrl.'&page='.$prevPage;?>" class="pageBtn"><img src="/images/buttons/btn_prev.gif" alt="Prev" border="0" /></a>
                                    <?php } else {?>
                                        <a href="##" class="pageBtn"><img src="/images/buttons/btn_first_null.gif" alt="First" border="0" /></a>
                                        <a href="##" class="pageBtn"><img src="/images/buttons/btn_prev_null.gif" alt="First" border="0" /></a>
                                    <?php }?>
                                    <?php if ($page!=$lastPage) {?>
                                        <a href="<?php echo $pageUrl.'&page='.$nextPage;?>" class="pageBtn"><img src="/images/buttons/btn_next.gif" alt="Next" border="0" /></a>
                                        <a href="<?php echo $pageUrl.'&page='.$lastPage;?>" class="pageBtn"><img src="/images/buttons/btn_last.gif" alt="Last" border="0" /></a>
                                    <?php } else {?>
                                        <a href="##" class="pageBtn"><img src="/images/buttons/btn_next_null.gif" alt="Next" border="0" /></a>
                                        <a href="##" class="pageBtn"><img src="/images/buttons/btn_last_null.gif" alt="Last" border="0" /></a>
                                    <?php }?>
                                    </dd>
                                </dl>
                            </td>
                        </tr>
                </table>
            </div>
<?php } else {?>
<?php }?>
        <div class="endChannel">
            <div style="clear:both;">
                <div style="float:left;width:300px;height:250px;overflow:hidden;">
                    <div id="googleAds_bottom"></div>
                </div>
                <div style="float:right;clear:right;width:300px;">
                    <div style="padding-bottom:10px; padding-top:5px;" class="googleLinkBlue"><br /></div>
                    <div style="font-size:12px;">
                        <p>
                        <a href="http://www.resumezapper.com/index.cfm?rz=367" target="_blank">Find Recruiters Here</a><br />
                        Distribute your resume directly to recruiters looking for candidates just like you. <br />
                        <a href="http://www.resumezapper.com/index.cfm?rz=367" target="_blank">www.resumezapper.com</a><br />
                        </p>

                        <p>
                        <a href="https://www.franchisegator.com/Selector/?industry=&amp;state=all&amp;investmentlevel=1000000&amp;CMP=EMC-amerijob&amp;adOrder=tip_0002&amp;adCreative=click_here&amp;utm_source=amerijob&amp;utm_medium=site&amp;utm_content=click_here" target="_blank">Be Your Own Boss</a><br />
                        Search the top self employment opportunities in your local area.<br />
                        <a href="https://www.franchisegator.com/Selector/?industry=&amp;state=all&amp;investmentlevel=1000000&amp;CMP=EMC-amerijob&amp;adOrder=tip_0002&amp;adCreative=click_here&amp;utm_source=amerijob&amp;utm_medium=site&amp;utm_content=click_here" target="_blank">www.franchisegator.com</a><br />
                        </p>
                    </div>
                </div>
                <br clear="all" />
            </div>
        </div>
    </div>

<div id="bubble_tooltip">
    <div class="bubble_top"><span></span></div>
    <div class="bubble_middle"><span id="bubble_tooltip_content"></span></div>
    <div class="bubble_bottom"></div>
</div>
  <script type='text/javascript' charset='utf-8'>(function(G,o,O,g,L,e){G[g]=G[g]||function(){(G[g]['q']=G[g]['q']||[]).push(arguments)},G[g]['t']=1*new Date;L=o.createElement(O),e=o.getElementsByTagName(O)[0];L.async=1;L.src='//www.google.com/adsense/search/async-ads.js';e.parentNode.insertBefore(L,e)})(window,document,'script','_googCsa');</script>
<script type="text/javascript" charset="utf-8">function getUpdated(obj, common, override) { for (var i in common) { obj[i] = (typeof override === 'object' && typeof override[i] !== 'undefined' ? override[i] : common[i]); } return obj; }var pageOptions = { 'adPage':'1','query':'<?php echo $keywords;?>','channel':'0530622515','pubId':'pub-2510281681734270' };
var adblock1 = { 'fontSizeDescription':'13px','colorBackground':'#ffffff','linkTarget':'_blank','attributionText':'Sponsored Links','titleBold':'0','number':'1','colorDomainLink':'#0074E0','fontSizeTitle':'15px','container':'googleAds_bottom','colorTitleLink':'#004B80' };
_googCsa('ads', pageOptions, adblock1 );</script>

@endsection



