    <div class="footer">

        <div class="searchFunctions">

            <div class="referOptions">
                <ul>
                    <li><a href="<?php echo $baseUrl;?>/my.job/searchJobsByState" title="Search United States Jobs By State" >Jobs By State</a></li>
                    <li><a href="<?php echo $baseUrl;?>/my.job/searchJobsByCategory" title="Search United States Jobs By Category" >Jobs By Category</a></li>
                    <li><a href="<?php echo $baseUrl;?>/my.job/searchJobsInTopCities" title="Search United States Jobs In Major Cities" >Jobs In Major Cities</a></li>
                    <li><a href="<?php echo $baseUrl;?>/my.job/searchJobsPopular" title="Search United States Jobs That Others Are Looking For" >Popular Job Searches</a></li>
                </ul>
            </div>
        </div>

        <br /><br />
        <img src="/images/spacer.gif" alt="" class="invisiShim" />
        <p class="footerLinks">
            <a href="<?php echo $baseUrl;?>/my.job/login" rel="nofollow">Log In</a>
            <em>|</em>
            <a href="/my.job/register" rel="nofollow">Register</a>
            <em>|</em>
            <a href="/my.job/aboutUs" >About Us</a>
            <em>|</em>
            <a href="/my.job/contactUs" rel="noFollow">Contact Us</a>
            <em>|</em>
            <a href="/my.job/privacyPolicy" rel="nofollow">Privacy Policy</a>
            <em>|</em>
            <a href="/my.job/termsOfUse" rel="noFollow">Terms of Use</a>
            <em>|</em>
            <a href="/my.job/browseJobs" >Browse Jobs</a>
            <em>|</em>
            <a href="/my.job/sitemap" >Site Map</a>
        </p>
        <p class="footerLinks" style="margin: 20px auto;padding:20px;">All content copyright &copy;1997 - 2018 AmericanJobs.com</p>
    </div>
