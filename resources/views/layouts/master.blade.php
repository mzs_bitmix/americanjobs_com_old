<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
@php
$baseUrl = env('APP_URL', 'https://www.americanjobs.com');
@endphp

<link rel="stylesheet" type="text/css" href="/css/style.css" />

<script type="text/javascript" >
window.onload = function() {
  if (navigator.appVersion.indexOf("MSIE")!=-1) {
    var field = document.getElementsByTagName("input");
    for(var i = 0; i < field.length; i++) {
      if (field[i].type == "text" || field[i].type == "password") {
        field[i].onfocus = function() {
          this.className += " focus";
        };
        field[i].onblur = function() {
          this.className = this.className.replace(/\bfocus\b/, "");
        };
      };
    };
field = null;
  };
};
</script>
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>American Jobs - @yield('title', 'Jobs in the United States')</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="keywords" content="@yield('meta-keywords','american jobs, jobs in america, jobs in the USA, jobs in the United States, search for jobs, apply to jobs, job opportunities, job search, internet jobs, america jobs, search jobs, find jobs, employment')" />
<meta name="description" content="@yield('meta-description', 'Search for jobs and apply to jobs in the United States. Over a million of the best American jobs are listed on AmericanJobs.com.  It\'s free and easy.')" />

<meta name="author" content="American Jobs (AmericanJobs.com)" />
<meta name="copyright" content="Copyright (c) 2001-2018 AmericanJobs.com" />
<meta name="language" content="en-us" />
<meta name="robots" content="index,follow" />
<meta name="GoogleBot" content="index,follow" />

<meta name="verify-v1" content="2nwJvZETRCUyX0pWb3pTUOVuKzMS+G9chFKCKdppQjc=" />

<link rel="Home" title="American Jobs (AmericanJobs.com) Homepage" href="<?php echo $baseUrl;?>" />
<link rel="shortcut icon" href="favicon.ico" />


@php
if (config('app.env') == 'production')
{
@endphp
    <script type="text/javascript">
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write("\<script src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'>\<\/script>" );
    </script>
    <script type="text/javascript">
    var pageTracker = _gat._getTracker("UA-1291750-15");
    pageTracker._initData();
    pageTracker._trackPageview();
    </script>

    <script type="text/javascript">
    var _comscore = _comscore || [];
    _comscore.push({ c1: "2", c2: "6035806"});
    (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
    })();
    </script>

        <noscript><img src="http://b.scorecardresearch.com/p?c1=2&c2=6035806&cv=2.0&cj=1" /></noscript>

@php
}
@endphp

<script type="text/javascript">
    var GB_ROOT_DIR = "<?php echo $baseUrl;?>/greybox/";
</script>

<script type="text/javascript" src="/greybox/AJS.js"></script>
<script type="text/javascript" src="/greybox/AJS_fx.js"></script>
<script type="text/javascript" src="/greybox/gb_scripts.js"></script>
<link href="/greybox/gb_styles.css" rel="stylesheet" type="text/css" />



</head>

<body>
<div id="uberContainer">

    <div class="returnUser">
@php
if ($viewName == 'login')
{} else {
@endphp
                <ul class="sliderInline">
                        <?php if (!Auth::check()) {?>
                        <li>Already have an account?<em>&nbsp;</em><a href="/login" rel="nofollow">Log in here</a>.</li>
                        <?php } else {?>
                        <li id="loggedIn"><a href="/my.job/account">My Account</a> | <a href="/logout">Log Out</a></li>
                        <?php } ?>
                </ul>
@php
}
@endphp
    </div>
    <div class="bodyContainer">
    @include('layouts.header')
    @yield('content')

    </div>

@include('layouts.footer')


</div>
</body>
</html>
