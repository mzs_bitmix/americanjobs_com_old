@php
if ($viewName == 'home')
{
@endphp
    <div class="headerMain">

        <h1 class="branding"><img src="/images/aj_headerLogo.gif" alt="AmericanJobs.com" border="0" /></h1>

    </div>
    <div style="float:right;margin-right:18px;margin-bottom:4px;">


                <div style="position:relative;top:0px;top:0px;right:0px;width:160px;*right:5px;margin-top: -8px;">

                    <script type="text/javascript">
                        addthis_url    = 'http://www.americanjobs.com/';
                        addthis_title  = 'American Jobs - Jobs in the United States';
                        addthis_brand = "AmericanJobs.com";
                        addthis_pub    = "vaweb2007";
                    </script>

                    <div class="addthis_toolbox addthis_default_style" style="float:right; margin-top:5px;">
                    <a class="addthis_button_facebook"></a>
                    <a class="addthis_button_delicious"></a>
                    <a class="addthis_button_email"></a>
                    <a class="addthis_button_favorites"></a>
                    <span class="addthis_separator">|</span>
                    <a href="http://addthis.com/bookmark.php?v=250&amp;pub=vaweb2007" class="addthis_button_expanded">More</a>
                    </div>
                    <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js##pub=vaweb2007"></script>
                </div>



    </div>
@php
} else {
@endphp
    <div class="headerSub">
        <h1 class="branding"><a href="/" rel="nofollow"><img src="/images/aj_headerLogo_sub.gif" alt="#request.app.siteCommonName#" border="0" /></a></h1>
    </div>
@php
}
@endphp
