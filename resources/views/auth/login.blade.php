@extends('layouts.master')

@section('content')
    @if ($errors->has('email'))
        <div class="validateErrors">
            <h4>Login failed:</h4>
            <ul>
                <li><em>{{ $errors->first('email') }}</em>  Please correct the error and try again.  </li>
                <li>If you have forgotten your password, please <a href="{{ route('password.request') }}">click here</a>.</li>
            </ul>
        </div>
    @endif
    @if ($errors->has('password'))
        <div class="validateErrors">
            <h4>Login failed:</h4>
            <ul>
                <li><em>{{ $errors->first('password') }}</em>  Please correct the error and try again.  </li>
                <li>If you have forgotten your password, please <a href="{{ route('password.request') }}">click here</a>.</li>
            </ul>
        </div>
    @endif
    <div class="slimReg" id="accountLead">
        <div class="loginForm">
            <div class="loginLeft">
            <form method="POST" action="{{ route('login') }}">
                @csrf
            <h3 class="loginHeader">Log in to your account</h3>
                <fieldset id="loginInfo">
                    <div class="formTier">
                        <div class="formSlotLeft">
                        <label class="std">{{ __('E-Mail Address') }}:</label>
                            <input type="text" name="email" class="formStd" tabindex="1" value="{{ old('email') }}" />
                        </div>
                        <div class="formSlot">
                        <label class="std">Password:</label>
                            <input type="password" name="password"  class="formStd" tabindex="2" />
                        </div>
                    </div>
                    <input type="hidden" name="rt" value="#rt#" />
                    <p style="display: inline; text-align: right; margin: 0; margin-bottom: 12px;"><input type="image" src="/images/btn_login_sm.gif" tabindex="3" style="border: 0; display: inline; float: right; *margin-right: 16px;" /><br />
                    <a class="btn btn-link" href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a></p>
                </fieldset>
            </form>
            </div>
            <div class="loginRight">
            <h3 class="loginHeader">Still not registered?</h3>
            <p>Start exploring your career potential by creating your free AmericanJobs.com account.</p>
            <p style="text-align: right;"><a class="red" href="/my.job/register">Get started &raquo;</a></p>
            </div>
        </div>
    </div>

@endsection
