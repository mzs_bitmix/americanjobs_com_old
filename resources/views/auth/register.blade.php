@extends('layouts.master')

@section('content')

<script type="text/javascript">
window.addEvent('domready', function() {
    //ADD FOCUS AND BLUR CLASSES TO IE BROWSER
    if (Browser.Engine.trident) {
        $(document).getElements('input[type!=radio]').addEvents({
            'focus': function(){
                this.addClass('focus');
            },
            'blur': function(){
                this.removeClass('focus');
            }
        });
    }

    //ADD ALERT TO USER WHEN MAXLENGTH REACHED
    $(document).getElements('input.validateMax[maxlength!=0]').addEvents({
        keypress:function(e){
            if(this.get('value').length >= this.get('maxlength') && e.key != 'tab' && e.key != 'backspace' && e.key != 'delete' && e.key != 'enter' && !this.selected){
                alert ('You have already entered the maximum of ' + this.get('maxlength') + ' characters for this field');
            }
            this.selected = false;
        },
        select:function(e){
            this.selected = true;
        },
        blur:function(){
            this.selected = false;
        }
    });
});

<?php if (isset($showResults) && $showResults == 1) {?>
function alternate(id){
 if(document.getElementsByTagName){
   var table = document.getElementById(id);
   var rows = "";

   if (table) rows = table.getElementsByTagName("tr");
   for(i = 0; i < rows.length; i++){
 //manipulate rows
     if(i % 2 == 0){
       rows[i].className = "even";
     }else{
       rows[i].className = "odd";
     }
   }
 }
}
<?php }?>

window.onload = function() {
  if (navigator.appVersion.indexOf("MSIE")!=-1) {4
    var field = document.getElementsByTagName("input");
    for(var i = 0; i < field.length; i++) {
      if (field[i].type == "text" || field[i].type == "password") {
        field[i].onfocus = function() {
          this.className += " focus";
        };
        field[i].onblur = function() {
          this.className = this.className.replace(/\bfocus\b/, "");
        };
      };
    };
field = null;
  };
};
</script>


</head>

<?php if (isset($showResults) && $showResults == 1) {?>
    <div class="initResults">
        <cfif showResults eq 1>
            <cfinclude template="/functions/getJ2CJobs.cfm" />

            <cfset resultCount = #searchResultCount(keywordField,geoField)# />
            <cfif resultCount gt 999> <cfset resultCount = "1000+" /> </cfif>
            <cfif resultCount gt 0>
                <cfoutput>
                <h2 class="resultsHeader">We found <em>#resultCount#</em> jobs that matched your search query.</h2>
                </cfoutput>
            <cfelse>
                <cflocation url="#request.app.websiteHome#/my.job/main/keywordField=#replace(keywordField,'/','~','ALL')#/geoField=#replace(geoField,'/','~','ALL')#/?results=0" addtoken="no" />
            </cfif>
            <cfset jobsReturned = getSearchResults(keywordField,geoField,0,#request.app.jobsPerPage#) />
            <cfswitch expression="#jobsReturned.recordCount#">
                <cfcase value="0">
                </cfcase>
                <cfcase value="1,2">
                    <h3 class="resultsListHeader">Here is a sample:</h3>
                </cfcase>
                <cfdefaultcase>
                    <h3 class="resultsListHeader">Here are the first few:</h3>
                </cfdefaultcase>
            </cfswitch>
            <cfif jobsReturned.recordCount gt 0>
            <div class="resultsContainer">
                <table id="jobsTeaser">
                    <cfoutput query="jobsReturned" maxRows="5">
                        <tr>
                            <td>
                                <dl>
                                    <dt><a href="##registration" onmouseover="showToolTip(event,'<strong>#jobsReturned.company#</strong><br />&##147;#reReplaceNoCase(jobsReturned.snippet, "['##""]|&quot;|[^[:alnum:][:punct:][ ]]", "", "ALL")#...&##148;<hr /><strong>Register to view and apply to this job</strong>!');return false" onmouseout="hideToolTip()">#jobsReturned.jobTitle#</a></dt>
                                    <dd>#jobsReturned.city# #jobsReturned.state#</dd>
                                </dl>
                            </td>
                        </tr>
                    </cfoutput>
                </table>
            </div>
        </cfif>

        </cfif>
<?php }?>
        <a name="registration"></a>
        <div class="slimReg slimReg2" id="registerLead">
            <h3 class="registrationHeader"><img src="/images/register_header.gif" alt=""></h3>
             <form method="POST" action="{{ route('register') }}">
                @csrf
                <input type="hidden" name="jobguid" value="">
                <p class="formLegend textRight"></p>
                <div class="doubleWide2">
                    <fieldset id="regContactInfo">
                        <div class="formTier2">
                            @if ($errors->has('email'))
                                <label class="std"><em>Email address: </em></label>
                            @else
                                <label class="std">Email address: </label>
                            @endif
                            <input type="text" name="email" class="formStd validateMax" tabindex="1" maxlength="90" value="{{ old('email') }}" />
                        </div>
                        <div class="formTier2">
                            @if ($errors->has('password'))
                                <label class="std"><em>Password: </em></label>
                            @else
                                <label class="std">Password: </label>
                            @endif
                            <input type="password" name="password" class="formStd validateMax" tabindex="2" maxlength="15" value="" />
                        </div>
                        <div class="formTier2">
                            @if ($errors->has('firstName'))
                                <label class="std"><em>First name: </em> </label>
                            @else
                                <label class="std">First name: </label>
                            @endif
                            <input type="text" name="firstName" class="formStd validateMax" tabindex="3" maxlength="40" value="{{ old('firstName') }}" />
                        </div>
                        <div class="formTier2">
                            @if ($errors->has('lastName'))
                                <label class="std"><em>Last name: </em></label>
                            @else
                                <label class="std">Last name: </label>
                            @endif
                            <input type="text" name="lastName"  class="formStd validateMax" tabindex="4" maxlength="50" value="{{ old('lastName') }}" />
                        </div>
                        <div class="formTier2">
                            @if ($errors->has('title'))
                                <label class="std"><em>Title: </em></label>
                            @else
                                <label class="std">Title: </label>
                            @endif
                            <input type="text" name="title"  class="formStd validateMax" maxlength = "80" tabindex="5" value="{{ old('title') }}" >
                        </div>

                        <div class="formTier2">
                            @if ($errors->has('zipCode'))
                                <label class="std"><em>Zip code: </em></label>
                            @else
                                <label class="std">Zip code: </label>
                            @endif
                            <input type="text" name="zipCode"  class="formStd validateMax" maxlength="5" tabindex="6" value="{{ old('zipCode') }}">
                        </div>

                        <div class="formTier2">
                            @if ($errors->has('jobCat'))
                                <label class="std"><em>Primary job category: </em></label>
                            @else
                                <label class="std">Primary job category: </label>
                            @endif
                            <select name="jobCat" tabindex="7">
                                <option value="0" selected>-- Please Select --</option>
                                @foreach($categoryList as $id=>$category)
                                    <option value="{{$id}}" <?php if ($id == old('jobCat')) { echo ' selected="selected"';}?>>{{$category}}</option>
                                @endforeach
                                <cfloop query="qryCategory">
                                    <option value="#categoryID#" <cfif jobCat eq categoryID>selected</cfif>>#categoryDesc#</option>
                                </cfloop>
                            </select>
                        </div>
                        <div class="formTier2">
                            @if ($errors->has('salarySelect'))
                                <label class="std"><em>Desired Salary: </em></label>
                            @else
                                <label class="std">Desired Salary: </label>
                            @endif
                            <select name="salarySelect" tabindex="8">
                                <option value="0" selected>-- Please Select a Salary Range --</option>
                                @foreach($salaryList as $salary)
                                    <option value="{{$salary['id']}}" <?php if ($salary['id'] == old('salarySelect')) { echo ' selected="selected"';}?>>{{$salary['salaryDesc']}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="formTier2">
                            @if ($errors->has('homePhone'))
                                <label class="std"><em>Home phone: </em></label>
                            @else
                                <label class="std">Phone: </label>
                            @endif
                            <input type="text" name="homePhone" class="formStd" tabindex="9" maxlength="13" value="{{ old('homePhone') }}" />
                        </div>
                        <div class="formTier2">
                            @if ($errors->has('ageEligible'))
                                <label class="std"><em>Are you 18 years of age or older?</em></label>
                            @else
                                <label class="std">Are you 18 years of age or older? </label>
                            @endif
                            <div class="radioBank">
                                <input type="radio" class="radioButton" name="ageEligible" value="Yes" tabindex="10" {{ old('ageEligible') == 'Yes' ? 'checked="checked"' : '' }} />Yes&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" class="radioButton" name="ageEligible" value="No" tabindex="11"  {{ old('ageEligible') == 'No' ? 'checked="checked"' : '' }} /> No
                            </div>
                        </div>


                        <div class="formTier2">
                            <label>
                                <input type="checkbox" class="checkbox" name="optin" id="optinemail" tabindex="12" value="YES" {{ old('optin') == 'YES' ? 'checked="checked"' : '' }}>Receive our job alerts and career tools email</label>
                        </div>
                    </fieldset>
                    <input type="image" class="getJobs2" src="/images/btn_register.gif" tabindex="27">
                </div>
                <input type="hidden" name="address1" value="#address1#" />
                <input type="hidden" name="address2" value="#address2#" />
                <input type="hidden" name="careerLevelSelect" value="#careerLevelSelect#" />
                <input type="hidden" name="city" value="#city#" />
                <input type="hidden" name="educationLevel" value="#educationLevel#" />
                <input type="hidden" name="mobilePhone" value="#mobilePhone#" />
                <input type="hidden" name="stateSelect" value="#stateSelect#" />
                <input type="hidden" name="workPhone" value="#workPhone#" />
                <input type="hidden" name="workPhoneExt" value="#workPhoneExt#" />
                <input type="hidden" name="workYearsSelect" value="#workYearsSelect#" />
            </form>
            <hr style="margin-top: 30px;">
        </div>

@endsection
