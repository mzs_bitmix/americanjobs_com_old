<?php

namespace App\Http\Controllers;

use Request;
use App\Domain\States as States;
use App\Domain\Category as Category;
use Auth;


/**
 * Page Controller
 * main controller for several page entries
 *
 */
class PageController extends Controller
{
    private $searchService;


    public function home()
    {
        return view('home',['viewName'=>'home']);
    }

    public function about()
    {
         return view('page.about',['viewName'=>'about']);
    }

    public function contact()
    {
         return view('page.contact',['viewName'=>'contact']);
    }

    public function privacy()
    {
         return view('page.privacy',['viewName'=>'privacy']);
    }

    public function terms()
    {
         return view('page.terms',['viewName'=>'terms']);
    }

    public function sitemap()
    {
        $stateDomain = new States();
        $categoryDomain = new Category();
        return view(
            'page.sitemap',
            [
                'viewName'=>'sitemap',
                'stateDomain'=>$stateDomain,
                'categoryDomain'=>$categoryDomain
            ]
        );
    }

    public function browsejobs()
    {
        $categoryDomain = new Category();
        $categoryList = $categoryDomain->getAlphaList();
        return view('page.browsejobs',
            [
                'viewName'=>'browsejobs',
                'categoryList'=>$categoryList,
            ]
        );
    }

    public function searchbycategory()
    {
        $categoryDomain = new Category();
        $categoryList = $categoryDomain->getAlphaList();
        return view(
            'page.searchbycategory',
            [
                'viewName'=>'searchbycategory',
                'categoryList'=>$categoryList,
            ]
        );
    }

    public function searchbystate()
    {
        $stateDomain = new States();
        $stateList = $stateDomain->getAlphaList();
        $metaKeywords = '';
        foreach ($stateList as $state) {
            $metaKeywords .= $state.' jobs,jobs in '.$state.',';
        }
        return view(
            'page.searchbystate',
            [
                'viewName'=>'searchbystate',
                'stateList'=>$stateList,
                'metaKeywords'=>trim($metaKeywords, ','),
            ]
        );
    }

    public function searchbytopcity()
    {
        $stateDomain = new States();
        $stateList = $stateDomain->getAlphaList();
        return view(
            'page.searchbytopcity',
            [
                'viewName'=>'searchbycity',
                'stateList'=>$stateList,
            ]
        );
    }

    public function searchbytoptitle()
    {
        return view(
            'page.searchbytoptitle',
            [
                'viewName'=>'searchbytoptitle',
                //'stateList'=>$stateList,
            ]
        );
    }

}
