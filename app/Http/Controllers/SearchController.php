<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Utility\Url;
use Bcismariu\Jobs2Careers\Jobs2Careers;

class SearchController extends Controller
{
    const jobsPerPage = 25;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Search jobs
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keywords = $request->get('keywordField');
        $location = $request->get('geoField');
        if ($request->has('page')) {
            $page = $request->get('page');
            $start = $page*self::jobsPerPage;
        } else {
            $page = 0;
            $start = 0;
        }
        if ($keywords!='' || $location != '') {
            $title = 'Job Search Results on AmericanJobs.com';
            $provider = new Jobs2Careers([
                'id' => (int)env('J2C_API_KEY', ''),
                'pass' => env('J2C_API_PASS', ''),
                'ip'    => $request->ip()
            ]);
            try {
                $results = $provider->setQuery($keywords)
                     ->setLocation($location)
                     ->setStart($start)
                     ->setLimit(self::jobsPerPage)
                     ->get();
                $resultCount = $results->count;
                $totalJobs = $results->total;
            } catch (\Exception $e) {
                $resultCount = 0;
                $totalJobs = 0;
            }
            if ( $resultCount == 0 && $totalJobs>0 ) {
                $resultCount = $totalJobs;
            }
            if ($resultCount>0) {
                $jobs = $results->jobs;
                if ( $resultCount == 1 ) {
                    $searchHeaderText = "We've found the job for you:";
                } else {
                    $searchHeaderText = "We've found ". number_format($resultCount). " jobs that you're looking for:";
                }
            } else {
                $jobs = array();
                $searchHeaderText = "We weren't able to find any jobs, please try again:";
            }

            if ( $page > 1 ) {
                $prevPage = $page - 1;
            } else {
                $prevPage = 0;
            }

            $displayResults = $totalJobs;
            $displayResultsStr = $displayResults;
            $maxJobsReturned = 0;

            $lastPage = (int)((($totalJobs - 1) / self::jobsPerPage) + 1)-1;
            if ( $page != $lastPage ) {
                $nextPage = $page + 1;
                $dispRange = ((($page)*self::jobsPerPage)+1)."-".(($page+1)*self::jobsPerPage);
            } else {
                $nextPage = $page+1;
                //$dispRange = ((($page-1)*self::jobsPerPage)+1)."-".((($maxJobsReturned?$page:($page-1))*self::jobsPerPage) + ($displayResults%self::jobsPerPage));
                $dispRange = $start."-".(($start+self::jobsPerPage)>$totalJobs?$totalJobs:($start+self::jobsPerPage));
            }
        } else {
            $jobs = array();
            $page = 0;
            $prevPage = 0;
            $nextPage = 0;
            $lastPage = 0;
            $dispRange = '';
            $displayResultsStr = '';
            $searchHeaderText = "We weren't able to find any jobs, please try again:";
        }
        $pageUrl = Url::jobSearchUrl($keywords, $location);
        return view('search.result',
            [
                'viewName'=>'search',
                'keywords'=>$keywords,
                'location'=>$location,
                'jobs'=>$jobs,
                'page'=>$page,
                'searchHeaderText'=>$searchHeaderText,
                'prevPage'=>$prevPage,
                'nextPage'=>$nextPage,
                'lastPage'=>$lastPage,
                'dispRange'=>$dispRange,
                'displayResultsStr'=>$displayResultsStr,
                'pageUrl'=>$pageUrl,
            ]
        );
    }
}
