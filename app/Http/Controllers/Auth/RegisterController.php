<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Profile;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Domain\Salary;
use App\Domain\Category;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        $category = new Category();
        $categoryList = $category->getList();
        $salary = new Salary();
        $salaryList = $salary->getList();
        return view(
            'auth.register',
            [
                'viewName'=>'register',
                'categoryList'=>$categoryList,
                'salaryList'=>$salaryList,
            ]
        );
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255|unique:americanjobs_jobseeker',
            'firstName' => 'required|string|max:255',
            'lastName' => 'required|string|max:255',
            'title' => 'required|string|max:255',
            'zipCode' => 'required|regex:/\b\d{5}\b/',
            'homePhone' => 'required|regex:/\(?\d{3}\)?[\s-]{0,}\d{3}[\s-]{0,}?\d{4}/',
            'password' => 'required|string|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['firstName'].' '.$data['lastName'],
            'jsFirstName' => $data['firstName'],
            'jsLastName' => $data['lastName'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'jsGUID' => $this->generateGUID(),
            'jsActive' => 1,
            'jsDateSubmitted' => date('Y-m-d H:i:s'),
            'jsLastUpdated' => date('Y-m-d H:i:s'),
            'jsZipcode' => $data['zipCode'],
            'jsIp' => request()->ip(),
            'jsHomePhone' => $data['homePhone']
        ]);
        if ($user->id) {
            $profile = Profile::create([
                'jsID' => $user->id,
                'profCurrentTitle' => $data['title'],
                'profDesiredSalary' => $data['salarySelect'],
                'profCareerLevel' => 0,
                'profWorkForce' => 0,
                'profAgeRange' => ($data['ageEligible']=='Yes'?1:0),
                'profWorkEligibility' => 1,
                'profEducation' => -1,
                'profLastUpdated' => date('Y-m-d H:i:s'),
            ]);
        }
        return $user;
    }

    protected function generateGUID()
    {
        $data = openssl_random_pseudo_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
}
