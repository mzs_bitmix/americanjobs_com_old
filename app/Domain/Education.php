<?php
namespace App\Domain;


/**
 * Domain Education list
 *
 */
class Education
{

    public function getList ()
    {
        return array (
            0 =>
            array (
                'id' => '1',
                'eduDesc' => 'No High School Diploma / HS Student',
                'eduPriority' => '1',
                'eduELearnersID' => '1',
                'eduJobComEduID' => '19',
            ),
            1 =>
            array (
                'id' => '2',
                'eduDesc' => 'High School or Equivalent',
                'eduPriority' => '2',
                'eduELearnersID' => '2',
                'eduJobComEduID' => '1',
            ),
            2 =>
            array (
                'id' => '3',
                'eduDesc' => 'Technical School',
                'eduPriority' => '3',
                'eduELearnersID' => '5',
                'eduJobComEduID' => '7',
            ),
            3 =>
            array (
                'id' => '4',
                'eduDesc' => 'Some College',
                'eduPriority' => '4',
                'eduELearnersID' => '3',
                'eduJobComEduID' => '2',
            ),
            4 =>
            array (
                'id' => '5',
                'eduDesc' => 'Associate\'s Degree',
                'eduPriority' => '5',
                'eduELearnersID' => '5',
                'eduJobComEduID' => '13',
            ),
            5 =>
            array (
                'id' => '6',
                'eduDesc' => 'Bachelor\'s Degree',
                'eduPriority' => '6',
                'eduELearnersID' => '6',
                'eduJobComEduID' => '3',
            ),
            6 =>
            array (
                'id' => '7',
                'eduDesc' => 'Master\'s Degree',
                'eduPriority' => '7',
                'eduELearnersID' => '7',
                'eduJobComEduID' => '5',
            ),
            7 =>
            array (
                'id' => '8',
                'eduDesc' => 'Doctoral Degree',
                'eduPriority' => '8',
                'eduELearnersID' => '8',
                'eduJobComEduID' => '6',
            ),
        );
    }

}
