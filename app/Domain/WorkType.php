<?php
namespace App\Domain;


/**
 * Domain Work type list
 *
 */
class WorkType
{

    public function getList ()
    {
        return array (
            0 =>
            array (
                'id' => '1',
                'WorkTypeVisible' => '1',
                'WorkTypePriority' => '1',
                'WorkTypeDesc' => 'Full-Time',
                'WorkTypeDescURL' => 'Full-Time',
            ),
            1 =>
            array (
                'id' => '2',
                'WorkTypeVisible' => '1',
                'WorkTypePriority' => '2',
                'WorkTypeDesc' => 'Part-Time',
                'WorkTypeDescURL' => 'Part-Time',
            ),
            2 =>
            array (
                'id' => '3',
                'WorkTypeVisible' => '1',
                'WorkTypePriority' => '3',
                'WorkTypeDesc' => 'Contract',
                'WorkTypeDescURL' => 'Contract',
            ),
            3 =>
            array (
                'id' => '4',
                'WorkTypeVisible' => '1',
                'WorkTypePriority' => '4',
                'WorkTypeDesc' => 'Consultant',
                'WorkTypeDescURL' => 'Consultant',
            ),
        );
    }

}
