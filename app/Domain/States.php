<?php
namespace App\Domain;


/**
 * Domain States list
 *
 */
class States
{

    public function getList ()
    {
        return array (
            array (
                'id' => 1,
                'StateCode' => 'AL',
                'StateName' => 'Alabama',
            ),
            array (
                'id' => 2,
                'StateCode' => 'AK',
                'StateName' => 'Alaska',
            ),
            array (
                'id' => 3,
                'StateCode' => 'AZ',
                'StateName' => 'Arizona',
            ),
            array (
                'id' => 4,
                'StateCode' => 'AR',
                'StateName' => 'Arkansas',
            ),
            array (
                'id' => 5,
                'StateCode' => 'CA',
                'StateName' => 'California',
            ),
            array (
                'id' => 6,
                'StateCode' => 'CO',
                'StateName' => 'Colorado',
            ),
            array (
                'id' => 7,
                'StateCode' => 'CT',
                'StateName' => 'Connecticut',
            ),
            array (
                'id' => 8,
                'StateCode' => 'DE',
                'StateName' => 'Delaware',
            ),
            array (
                'id' => 9,
                'StateCode' => 'DC',
                'StateName' => 'Dist of Columbia',
            ),
            array (
                'id' => 10,
                'StateCode' => 'FL',
                'StateName' => 'Florida',
            ),
            array (
                'id' => 11,
                'StateCode' => 'GA',
                'StateName' => 'Georgia',
            ),
            array (
                'id' => 12,
                'StateCode' => 'HI',
                'StateName' => 'Hawaii',
            ),
            array (
                'id' => 13,
                'StateCode' => 'ID',
                'StateName' => 'Idaho',
            ),
            array (
                'id' => 14,
                'StateCode' => 'IL',
                'StateName' => 'Illinois',
            ),
            array (
                'id' => 15,
                'StateCode' => 'IN',
                'StateName' => 'Indiana',
            ),
            array (
                'id' => 16,
                'StateCode' => 'IA',
                'StateName' => 'Iowa',
            ),
            array (
                'id' => 17,
                'StateCode' => 'KS',
                'StateName' => 'Kansas',
            ),
            array (
                'id' => 18,
                'StateCode' => 'KY',
                'StateName' => 'Kentucky',
            ),
            array (
                'id' => 19,
                'StateCode' => 'LA',
                'StateName' => 'Louisiana',
            ),
            array (
                'id' => 20,
                'StateCode' => 'ME',
                'StateName' => 'Maine',
            ),
            array (
                'id' => 21,
                'StateCode' => 'MD',
                'StateName' => 'Maryland',
            ),
            array (
                'id' => 22,
                'StateCode' => 'MA',
                'StateName' => 'Massachusetts',
            ),
            array (
                'id' => 23,
                'StateCode' => 'MI',
                'StateName' => 'Michigan',
            ),
            array (
                'id' => 24,
                'StateCode' => 'MN',
                'StateName' => 'Minnesota',
            ),
            array (
                'id' => 25,
                'StateCode' => 'MS',
                'StateName' => 'Mississippi',
            ),
            array (
                'id' => 26,
                'StateCode' => 'MO',
                'StateName' => 'Missouri',
            ),
            array (
                'id' => 27,
                'StateCode' => 'MT',
                'StateName' => 'Montana',
            ),
            array (
                'id' => 28,
                'StateCode' => 'NE',
                'StateName' => 'Nebraska',
            ),
            array (
                'id' => 29,
                'StateCode' => 'NV',
                'StateName' => 'Nevada',
            ),
            array (
                'id' => 30,
                'StateCode' => 'NH',
                'StateName' => 'New Hampshire',
            ),
            array (
                'id' => 31,
                'StateCode' => 'NJ',
                'StateName' => 'New Jersey',
            ),
            array (
                'id' => 32,
                'StateCode' => 'NM',
                'StateName' => 'New Mexico',
            ),
            array (
                'id' => 33,
                'StateCode' => 'NY',
                'StateName' => 'New York',
            ),
            array (
                'id' => 34,
                'StateCode' => 'NC',
                'StateName' => 'North Carolina',
            ),
            array (
                'id' => 35,
                'StateCode' => 'ND',
                'StateName' => 'North Dakota',
            ),
            array (
                'id' => 36,
                'StateCode' => 'OH',
                'StateName' => 'Ohio',
            ),
            array (
                'id' => 37,
                'StateCode' => 'OK',
                'StateName' => 'Oklahoma',
            ),
            array (
                'id' => 38,
                'StateCode' => 'OR',
                'StateName' => 'Oregon',
            ),
            array (
                'id' => 39,
                'StateCode' => 'PA',
                'StateName' => 'Pennsylvania',
            ),
            array (
                'id' => 40,
                'StateCode' => 'RI',
                'StateName' => 'Rhode Island',
            ),
            array (
                'id' => 41,
                'StateCode' => 'SC',
                'StateName' => 'South Carolina',
            ),
            array (
                'id' => 42,
                'StateCode' => 'SD',
                'StateName' => 'South Dakota',
            ),
            array (
                'id' => 43,
                'StateCode' => 'TN',
                'StateName' => 'Tennessee',
            ),
            array (
                'id' => 44,
                'StateCode' => 'TX',
                'StateName' => 'Texas',
            ),
            array (
                'id' => 45,
                'StateCode' => 'UT',
                'StateName' => 'Utah',
            ),
            array (
                'id' => 46,
                'StateCode' => 'VT',
                'StateName' => 'Vermont',
            ),
            array (
                'id' => 47,
                'StateCode' => 'VA',
                'StateName' => 'Virginia',
            ),
            array (
                'id' => 48,
                'StateCode' => 'WA',
                'StateName' => 'Washington',
            ),
            array (
                'id' => 49,
                'StateCode' => 'WV',
                'StateName' => 'West Virginia',
            ),
            array (
                'id' => 50,
                'StateCode' => 'WI',
                'StateName' => 'Wisconsin',
            ),
            array (
                'id' => 51,
                'StateCode' => 'WY',
                'StateName' => 'Wyoming',
            ),
        );

    }

    public function getAlphaList()
    {
        $list = $this->getList();
        $states = array();
        foreach ($list as $key=>$state) {
            $states[$state['id']] = $state['StateName'];
        }
        asort($states);
        return $states;

    }

}
