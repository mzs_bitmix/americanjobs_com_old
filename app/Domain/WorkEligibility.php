<?php
namespace App\Domain;


/**
 * Domain Work Eligibility list
 *
 */
class WorkEligibility
{

    public function getList ()
    {
        return array (
            0 =>
            array (
                'id' => '1',
                'workEligibilityDesc' => 'I can work for any employer in the United States',
            ),
            1 =>
            array (
                'id' => '2',
                'workEligibilityDesc' => 'I can work for my present employer in the United States',
            ),
            2 =>
            array (
                'id' => '3',
                'workEligibilityDesc' => 'I require sponsorship to work in the United States',
            ),
        );
    }

}
