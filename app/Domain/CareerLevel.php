<?php
namespace App\Domain;


/**
 * Domain Career Level list
 *
 */
class CareerLevel
{

    public function getList ()
    {
        return array (
            0 =>
            array (
                'id' => '1',
                'careerLevelDesc' => 'Student (Undergrad/Grad)',
            ),
            1 =>
            array (
                'id' => '2',
                'careerLevelDesc' => 'Entry Level (<2 Yrs experience)',
            ),
            2 =>
            array (
                'id' => '3',
                'careerLevelDesc' => 'Mid Career (>2 Yrs experience)',
            ),
            3 =>
            array (
                'id' => '4',
                'careerLevelDesc' => 'Mid Level Management',
            ),
            4 =>
            array (
                'id' => '5',
                'careerLevelDesc' => 'Executive',
            ),
            4 =>
            array (
                'id' => '6',
                'careerLevelDesc' => 'Senior Executive',
            ),
        );

    }

}
