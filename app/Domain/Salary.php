<?php
namespace App\Domain;


/**
 * Domain Salary list
 *
 */
class Salary
{

    public function getList ()
    {
        return array (
            0 =>
            array (
                'id' => '1',
                'salaryLow' => '0',
                'salaryHigh' => '19999',
                'salaryDesc' => 'Less Than $20k',
            ),
            1 =>
            array (
                'id' => '2',
                'salaryLow' => '20000',
                'salaryHigh' => '29999',
                'salaryDesc' => '$20k to $29k',
            ),
            2 =>
            array (
                'id' => '3',
                'salaryLow' => '30000',
                'salaryHigh' => '39999',
                'salaryDesc' => '$30k to $39k',
            ),
            3 =>
            array (
                'id' => '4',
                'salaryLow' => '40000',
                'salaryHigh' => '49999',
                'salaryDesc' => '$40k to $49k',
            ),
            4 =>
            array (
                'id' => '5',
                'salaryLow' => '50000',
                'salaryHigh' => '59999',
                'salaryDesc' => '$50k to $59k',
            ),
            5 =>
            array (
                'id' => '6',
                'salaryLow' => '60000',
                'salaryHigh' => '69999',
                'salaryDesc' => '$60k to $69k',
            ),
            6 =>
            array (
                'id' => '7',
                'salaryLow' => '70000',
                'salaryHigh' => '79999',
                'salaryDesc' => '$70k to $79k',
            ),
            7 =>
            array (
                'id' => '8',
                'salaryLow' => '80000',
                'salaryHigh' => '89999',
                'salaryDesc' => '$80k to $89k',
            ),
            8 =>
            array (
                'id' => '9',
                'salaryLow' => '90000',
                'salaryHigh' => '99999',
                'salaryDesc' => '$90k to $99k',
            ),
            9 =>
            array (
                'id' => '10',
                'salaryLow' => '100000',
                'salaryHigh' => '109999',
                'salaryDesc' => '$100k to $109k',
            ),
            10 =>
            array (
                'id' => '11',
                'salaryLow' => '110000',
                'salaryHigh' => '119999',
                'salaryDesc' => '$110k to $119k',
            ),
            11 =>
            array (
                'id' => '12',
                'salaryLow' => '120000',
                'salaryHigh' => '129999',
                'salaryDesc' => '$120k to $129k',
            ),
            12 =>
            array (
                'id' => '13',
                'salaryLow' => '130000',
                'salaryHigh' => '139999',
                'salaryDesc' => '$130k to $139k',
            ),
            13 =>
            array (
                'id' => '14',
                'salaryLow' => '140000',
                'salaryHigh' => '149999',
                'salaryDesc' => '$140k to $149k',
            ),
            14 =>
            array (
                'id' => '15',
                'salaryLow' => '150000',
                'salaryHigh' => '174999',
                'salaryDesc' => '$150k to $174k',
            ),
            15 =>
            array (
                'id' => '16',
                'salaryLow' => '175000',
                'salaryHigh' => '199999',
                'salaryDesc' => '$175k to $199k',
            ),
            16 =>
            array (
                'id' => '17',
                'salaryLow' => '200000',
                'salaryHigh' => '224999',
                'salaryDesc' => '$200k to $225k',
            ),
            17 =>
            array (
                'id' => '18',
                'salaryLow' => '226000',
                'salaryHigh' => '1000000',
                'salaryDesc' => 'More than $225k',
            ),
        );
    }

}
