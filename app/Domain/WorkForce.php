<?php
namespace App\Domain;


/**
 * Domain Work Force list
 *
 */
class WorkForce
{

    public function getList ()
    {
        return array (
            0 =>
            array (
                'id' => '1',
                'workForceDesc' => '0-4 Years',
            ),
            1 =>
            array (
                'id' => '2',
                'workForceDesc' => '5-10 Years',
            ),
            2 =>
            array (
                'id' => '3',
                'workForceDesc' => '11-15 Years',
            ),
            3 =>
            array (
                'id' => '4',
                'workForceDesc' => '16-20 Years',
            ),
            4 =>
            array (
                'id' => '5',
                'workForceDesc' => '21+ Years',
            ),
        );

    }

}
