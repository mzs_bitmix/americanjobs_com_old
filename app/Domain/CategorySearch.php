<?php
namespace App\Domain;


/**
 * Domain category Search
 *
 */
class CategorySearch
{

    public function getList ()
    {
        return array (
            0 =>
            array (
                'id' => '1',
                'csDesc' => 'Accounting',
            ),
            1 =>
            array (
                'id' => '2',
                'csDesc' => 'Actuarial/Statistics',
            ),
            2 =>
            array (
                'id' => '3',
                'csDesc' => 'Administrative/Clerical',
            ),
            3 =>
            array (
                'id' => '4',
                'csDesc' => 'Advertising',
            ),
            4 =>
            array (
                'id' => '5',
                'csDesc' => 'Aerospace/Defense',
            ),
            5 =>
            array (
                'id' => '6',
                'csDesc' => 'Agriculture/Fisheries',
            ),
            6 =>
            array (
                'id' => '7',
                'csDesc' => 'Apparel/Fashion',
            ),
            7 =>
            array (
                'id' => '8',
                'csDesc' => 'Architecture',
            ),
            8 =>
            array (
                'id' => '9',
                'csDesc' => 'Arts/Culture',
            ),
            9 =>
            array (
                'id' => '10',
                'csDesc' => 'Automotive',
            ),
            10 =>
            array (
                'id' => '11',
                'csDesc' => 'Banking',
            ),
            11 =>
            array (
                'id' => '12',
                'csDesc' => 'Biotech/Biology',
            ),
            12 =>
            array (
                'id' => '13',
                'csDesc' => 'Computing',
            ),
            13 =>
            array (
                'id' => '14',
                'csDesc' => 'Computing - Hardware',
            ),
            14 =>
            array (
                'id' => '15',
                'csDesc' => 'Computing - Internet',
            ),
            15 =>
            array (
                'id' => '16',
                'csDesc' => 'Computing - Software',
            ),
            16 =>
            array (
                'id' => '17',
                'csDesc' => 'Construction',
            ),
            17 =>
            array (
                'id' => '18',
                'csDesc' => 'Customer Service/Call Center',
            ),
            18 =>
            array (
                'id' => '19',
                'csDesc' => 'Education/Training',
            ),
            19 =>
            array (
                'id' => '20',
                'csDesc' => 'Engineering',
            ),
            20 =>
            array (
                'id' => '21',
                'csDesc' => 'Engineering - Chemical',
            ),
            21 =>
            array (
                'id' => '22',
                'csDesc' => 'Engineering - Civil',
            ),
            22 =>
            array (
                'id' => '23',
                'csDesc' => 'Engineering - Electrical',
            ),
            23 =>
            array (
                'id' => '24',
                'csDesc' => 'Engineering - General',
            ),
            24 =>
            array (
                'id' => '25',
                'csDesc' => 'Engineering - Industrial',
            ),
            25 =>
            array (
                'id' => '26',
                'csDesc' => 'Engineering - Mechanical',
            ),
            26 =>
            array (
                'id' => '27',
                'csDesc' => 'Engineering - Mineral',
            ),
            27 =>
            array (
                'id' => '28',
                'csDesc' => 'Engineering - Municipal',
            ),
            28 =>
            array (
                'id' => '29',
                'csDesc' => 'Engineering - Structural',
            ),
            29 =>
            array (
                'id' => '30',
                'csDesc' => 'Environmental',
            ),
            30 =>
            array (
                'id' => '31',
                'csDesc' => 'Finance',
            ),
            31 =>
            array (
                'id' => '32',
                'csDesc' => 'Forestry',
            ),
            32 =>
            array (
                'id' => '33',
                'csDesc' => 'Geology/Geography',
            ),
            33 =>
            array (
                'id' => '34',
                'csDesc' => 'Government',
            ),
            34 =>
            array (
                'id' => '35',
                'csDesc' => 'Graphic Arts',
            ),
            35 =>
            array (
                'id' => '36',
                'csDesc' => 'Health/Medical',
            ),
            36 =>
            array (
                'id' => '37',
                'csDesc' => 'Health/Medical - Dental',
            ),
            37 =>
            array (
                'id' => '38',
                'csDesc' => 'Health/Medical - Doctors',
            ),
            38 =>
            array (
                'id' => '39',
                'csDesc' => 'Health/Medical - General',
            ),
            39 =>
            array (
                'id' => '40',
                'csDesc' => 'Health/Medical - Health Records',
            ),
            40 =>
            array (
                'id' => '41',
                'csDesc' => 'Health/Medical - Laboratory',
            ),
            41 =>
            array (
                'id' => '42',
                'csDesc' => 'Health/Medical - Nursing',
            ),
            42 =>
            array (
                'id' => '43',
                'csDesc' => 'Health/Medical - Radiology &amp; Imaging',
            ),
            43 =>
            array (
                'id' => '44',
                'csDesc' => 'Health/Medical - Rehabilitation',
            ),
            44 =>
            array (
                'id' => '45',
                'csDesc' => 'Health/Medical - Social Services',
            ),
            45 =>
            array (
                'id' => '46',
                'csDesc' => 'Health/Medical - Support Services',
            ),
            46 =>
            array (
                'id' => '47',
                'csDesc' => 'Hospitality/Tourism',
            ),
            47 =>
            array (
                'id' => '48',
                'csDesc' => 'Human Resource',
            ),
            48 =>
            array (
                'id' => '49',
                'csDesc' => 'Industrial Design/Drafting',
            ),
            49 =>
            array (
                'id' => '50',
                'csDesc' => 'Insurance',
            ),
            50 =>
            array (
                'id' => '51',
                'csDesc' => 'Law Enforcement/Security',
            ),
            51 =>
            array (
                'id' => '52',
                'csDesc' => 'Legal',
            ),
            52 =>
            array (
                'id' => '53',
                'csDesc' => 'Librarians',
            ),
            53 =>
            array (
                'id' => '54',
                'csDesc' => 'Logistics/Supply Chain',
            ),
            54 =>
            array (
                'id' => '55',
                'csDesc' => 'Manufacturing/Operations',
            ),
            55 =>
            array (
                'id' => '56',
                'csDesc' => 'Marketing',
            ),
            56 =>
            array (
                'id' => '57',
                'csDesc' => 'Media/Publishing',
            ),
            57 =>
            array (
                'id' => '58',
                'csDesc' => 'Metals',
            ),
            58 =>
            array (
                'id' => '59',
                'csDesc' => 'Mining',
            ),
            59 =>
            array (
                'id' => '60',
                'csDesc' => 'Multimedia/Web Design',
            ),
            60 =>
            array (
                'id' => '61',
                'csDesc' => 'Non-Profit',
            ),
            61 =>
            array (
                'id' => '62',
                'csDesc' => 'Oil &amp; Gas/Utilities',
            ),
            62 =>
            array (
                'id' => '63',
                'csDesc' => 'Packaging',
            ),
            63 =>
            array (
                'id' => '64',
                'csDesc' => 'Pharmaceutical',
            ),
            64 =>
            array (
                'id' => '65',
                'csDesc' => 'Plastics',
            ),
            65 =>
            array (
                'id' => '66',
                'csDesc' => 'Printing',
            ),
            66 =>
            array (
                'id' => '67',
                'csDesc' => 'Public Relations',
            ),
            67 =>
            array (
                'id' => '68',
                'csDesc' => 'Pulp &amp; Paper',
            ),
            68 =>
            array (
                'id' => '69',
                'csDesc' => 'Purchasing',
            ),
            69 =>
            array (
                'id' => '70',
                'csDesc' => 'Quality Control',
            ),
            70 =>
            array (
                'id' => '71',
                'csDesc' => 'Real Estate/Property Management',
            ),
            71 =>
            array (
                'id' => '72',
                'csDesc' => 'Religious',
            ),
            72 =>
            array (
                'id' => '73',
                'csDesc' => 'Retail',
            ),
            73 =>
            array (
                'id' => '74',
                'csDesc' => 'Sales',
            ),
            74 =>
            array (
                'id' => '75',
                'csDesc' => 'Science',
            ),
            75 =>
            array (
                'id' => '76',
                'csDesc' => 'Sports/Recreation',
            ),
            76 =>
            array (
                'id' => '77',
                'csDesc' => 'Telecom',
            ),
            77 =>
            array (
                'id' => '78',
                'csDesc' => 'Trade Shows',
            ),
            78 =>
            array (
                'id' => '79',
                'csDesc' => 'Trades/Technicians',
            ),
            79 =>
            array (
                'id' => '80',
                'csDesc' => 'Translation',
            ),
            80 =>
            array (
                'id' => '81',
                'csDesc' => 'Transportation',
            ),
            81 =>
            array (
                'id' => '82',
                'csDesc' => 'Veterinary/Animal Sciences',
            ),
        );

    }

}
