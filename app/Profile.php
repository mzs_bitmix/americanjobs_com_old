<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'americanjobs_jobseekerprofile';
    public $timestamps = false;
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'jsID';

    protected $fillable = [
        'jsID', 'profCurrentTitle', 'profDesiredSalary', 'profCareerLevel', 'profWorkForce', 'profAgeRange', 'profWorkEligibility', 'profEducation', 'profLastUpdated'
    ];
}
