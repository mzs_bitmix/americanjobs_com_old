<?php
namespace App\Utility;


/**
 * Url utility
 * anything relatd to urls
 *
 */
class Url
{

    /**
     * create a job search link
     */
    public static function jobSearchUrl ($keywords, $location)
    {
        return '/my.job/searchJobs/?keywordField='.$keywords.'&geoField='.$location;
    }

}
