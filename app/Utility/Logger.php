<?php
namespace App\Utility;


use Monolog\Logger as MLogger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\NativeMailerHandler;
/**
 * Logger utility
 *
 */
class Logger
{
    protected static $_instance;
    protected static $_logFileName = 'zillion.log';

    /**
     * get a logger system instance
     * @return Logger_System
     */
    public static function getInstance ()
    {
        if (!isset(self::$_instance)) {
            self::$_instance = new MLogger('zillion');
            self::$_instance->pushHandler(new StreamHandler(storage_path('logs/'.self::$_logFileName)), MLogger::DEBUG);
            self::$_instance->pushHandler(
                    new NativeMailerHandler(
                        'install@bitmixsoft.com',
                        'Critical Error in ZillionResumes',
                        env('MAIL_FROM_ADDRESS', 'install@bitmixsoft.com'),
                        MLogger::ALERT
                    )
                );
        }
        return self::$_instance;
    }

    public static function setInstance(Logger_System $logger)
    {
        self::$_instance = $logger;
    }

    public static function setLogFileName($logfileName)
    {
        self::$_logFileName = $logfileName;
    }

    /**
     * return a logger object
     * @return Logger_System
     */
    public static function getLogger()
    {
        return self::getInstance();
    }

    public static function log($level, $message, array $context=array())
    {
        $logger = self::getInstance();
        $logger->log($level, $message, $context);
    }

    public static function debug($message, array $context=array())
    {
        $logger = self::getInstance();
        $logger->debug($message, $context);
    }

    public static function info($message, array $context=array())
    {
        $logger = self::getInstance();
        $logger->info($message, $context);
    }

    public static function notice($message, array $context=array())
    {
        $logger = self::getInstance();
        $logger->notice($message, $context);
    }

    public static function warning($message, array $context=array())
    {
        $logger = self::getInstance();
        $logger->warning($message, $context);
    }

    public static function error($message, array $context=array())
    {
        $logger = self::getInstance();
        $logger->error($message, $context);
    }

    public static function critical($message, array $context=array())
    {
        $logger = self::getInstance();
        $logger->critical($message, $context);
    }

    public static function alert($message, array $context=array())
    {
        $logger = self::getInstance();
        $logger->alert($message, $context);
    }

    public static function emergency($message, array $context=array())
    {
        $logger = self::getInstance();
        $logger->emergency($message, $context);
    }
}
