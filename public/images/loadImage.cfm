<cfparam name = "mi" default = "0">  <!--- memberID ---> <cfset mi = trim(decodeURL(mi)) />
<cfparam name = "si" default = "1">  <!--- source id, default is JF --->  <cfset si = trim(decodeURL(si)) />

<cfquery name="getJSID" datasource="#request.app.dsMain#">
select jsid  
from tblJobseeker where 
jsguid = <cfqueryparam cfsqltype="CF_SQL_TEXT" value="#mi#" />
</cfquery>

<cftry>
<!--- 
    <cfif not findNoCase("GoogleImageProxy", cgi.http_user_agent)>
         --->
        <cfquery name = "trackJsLastEmailOpen" datasource="#request.app.dsMain#">
            insert into tblJobseekerEmailAction (issueSource, jsid, ipAddress, userAgent)
            values (<cfqueryparam value="#variables.si#" cfsqltype="cf_sql_integer" />, 
            		<cfqueryparam value="#getJSID.jsid#" cfsqltype="cf_sql_integer" />, 
            		<cfqueryparam value="#cgi.remote_addr#" cfsqltype="cf_sql_varchar" />, 
                    <cfqueryparam value="#cgi.http_user_agent#" cfsqltype="cf_sql_varchar" />)
        </cfquery>
<!--- 
    </cfif> --->

<cfcatch><!--- unique index to prevent multiple inserts for js / issue combination ---></cfcatch>
</cftry>

<cftry>
   
<!---     <cfif not findNoCase("GoogleImageProxy", cgi.http_user_agent)> --->
        <cfquery name = "trackJsLastEmailOpen" datasource="#request.app.dsMain#">
            UPDATE varJobseekerActivity
            SET    jsaLastEmailOpen = getDate()
            WHERE  jsid = <cfqueryparam value="#getJSID.jsid#" cfsqltype="cf_sql_integer" />
        </cfquery>
<!---     </cfif> --->

<cfcatch><!--- unique index to prevent multiple inserts for js / issue combination ---></cfcatch>
</cftry>

<cfcontent type="image/gif" file="c:\html\americanjobs.com\images\ico-x1.gif" deletefile="No">
</cfcontent>